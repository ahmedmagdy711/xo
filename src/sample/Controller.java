package sample;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import java.util.*;

public class Controller {

    @FXML
    private GridPane grid;

    Button[][] buttons = new Button[3][3];

    int[][] gameArray = new int[3][3];



    @FXML
    protected void initialize(){
        Game game = new Game(gameArray);
        boolean[] player = {true};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j] = new Button();
                buttons[i][j].setId("" + i +  "_" + j);
                buttons[i][j].setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        String[] index = ((Button)(event.getSource())).getId().split("_");
                        int i = Integer.parseInt(index[0]);
                        int j = Integer.parseInt(index[1]);
                        if(gameArray[i][j] != 0) return;
                        game.play(i, j, -1);
                        gameArray = game.root.getState().getGameArray();

                        for (int k = 0; k < 3; k++) {
                            for (int l = 0; l < 3; l++) {
                                if(gameArray[k][l] == -1) {
                                    buttons[k][l].setText("X");
                                }else if(gameArray[k][l] == 1){
                                    buttons[k][l].setText("O");
                                }
                            }
                        }
                    }
                });
                buttons[i][j].setMinHeight(70);
                buttons[i][j].setMinWidth(70);
                grid.add(buttons[i][j], j, i);
            }
        }
    }




}
