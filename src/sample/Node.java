package sample;

import java.util.ArrayList;

/**
 * Created by ahmed on 5/5/16.
 */
public class Node {
    State state;
    ArrayList<Node> children = new ArrayList<>();
    int player;
    public Node(State state, int player) {
        this.state = state;
        this.player = player;
    }

    public State getState() {
        return state;
    }

    public void addState(State state, int player) {
        children.add(new Node(state, player));
    }
    public void addState(Node node) {
        children.add(node);
    }

    public ArrayList<Node> getChildren() {
        return children;
    }
}
