package sample;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by ahmed on 5/5/16.
 */
public class Game {
    Node root ;
    int leaf = 0;
    Game(int[][] gameArray){
        root = new Node(new State(gameArray), 1);
        long test = System.currentTimeMillis();
//        System.out.println("" + System.currentTimeMillis());
        buildTree();
//        calBest( root.getChildren());
//        System.out.println("" + (float)( System.currentTimeMillis() - test ) / 1000);
//        System.out.println(leaf);
    }

    void play(int i, int j, int player) {
        if(searchTree(getNewGameArray(i, j, player, root.getState().getGameArray()))!= null)
            root = searchTree(getNewGameArray(i, j, player, root.getState().getGameArray()));
        if(root.getChildren().size() != 0) {
            calBest( root.getChildren());
        }
    }

    void calBest(ArrayList<Node> children) {
        int values[] = new int[children.size()];
        Arrays.fill(values, Integer.MIN_VALUE);
        int max = Integer.MIN_VALUE, maxIndex = -1;
        for (int i = 0; i < children.size(); i++) {
            values[i] = getPathValue(children.get(i), true);
            System.out.println(values[i]);
            printArr(children.get(i).getState().getGameArray());
            if(max != Math.max(max, values[i])) {
                max = values[i];
                maxIndex = i;
            }
        }
        System.out.println(max);
        root = children.get(maxIndex);
    }

    int getPathValue(Node node, boolean max ) {
        if(node.getChildren().size() == 0) {
            return node.getState().winner;
        }
        if (!max) {
            int maxvalue = Integer.MIN_VALUE;
            for (int i = 0; i < node.getChildren().size(); i++) {
                maxvalue = Math.max(getPathValue(node.getChildren().get(i), !max ), maxvalue);
                if(maxvalue == 1) return maxvalue;
            }
            return maxvalue;
        }else {
            int minvalue = Integer.MAX_VALUE;
            for (int i = 0; i < node.getChildren().size(); i++) {
                minvalue = Math.min(getPathValue(node.getChildren().get(i), !max), minvalue);
                if(minvalue == -1) return minvalue;
            }
            return minvalue;
        }
    }

    void buildTree() {
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);
        while(!nodes.isEmpty()){
            Node node = nodes.poll();
            if(node.getChildren().size() == 0)
                createChildren(node);
//            System.out.println(node.getChildren().size());
            for (int i = 0; i < node.getChildren().size(); i++) {
                nodes.add(node.getChildren().get(i));
            }
        }
    }

    private void createChildren(Node node) {
        if(node.getState().finished) { leaf++; return;}
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(node.getState().getGameArray()[i][j] == 0) {
                    if(node.player == -1) {
//                        Node temp = searchTree(getNewGameArray(i, j, -1, node.getState().getGameArray()));
//                        System.out.println("here");
//                        if(temp != null)
//                            node.addState(temp);
//                        else
                            node.addState(new State(getNewGameArray(i, j, 1, node.getState().getGameArray())),
                                1);
                    }
                    else {
//                        Node temp = searchTree(getNewGameArray(i, j, -1, node.getState().getGameArray()));
//                        if(temp != null)
//                            node.addState(temp);
//                        else
                            node.addState(new State(getNewGameArray(i, j, -1, node.getState().getGameArray())),
                                -1);
                    }
                }
            }
        }
    }

    private void printArr(int [][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println("");
        }
        System.out.println("=====================");
    }

    private int[][] getNewGameArray(int in, int jn, int value , int[][] arr) {
        int [][] array = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                array[i][j] = arr[i][j];
                if(in == i && jn == j)
                    array[i][j] = value;
            }
        }
        return array;
    }

    private Node searchTree(int [][] arr) {
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);
        while(!nodes.isEmpty()){
            Node node = nodes.poll();
//            System.out.println(comp(node.getState().getGameArray(), arr));
            if(comp(node.getState().getGameArray(), arr)) {
                return node;
            }
            for (int i = 0; i < node.getChildren().size(); i++) {
                nodes.add(node.getChildren().get(i));
            }
        }
        return null;
    }

    private boolean comp(int[][]arr1 ,int[][]arr2) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(arr1[i][j] != arr2[i][j]) return false;
            }
        }
        return true;
    }

}