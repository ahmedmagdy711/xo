package sample;

/**
 * Created by ahmed on 5/5/16.
 */
class State{
    int[][] gameArray;
    boolean finished = true;
    int winner = 0;
    public State(int[][] gameArray) {
        this.gameArray = gameArray;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(gameArray[i][j] == 0)
                    finished = false;
            }
        }
        winner = winner();
    }

    public int[][] getGameArray() {
        return gameArray;
    }

    public boolean isFinished(){
        return finished;
    }

    public int result () {
        return winner;
    }

    private int winner() {
        if(checkRows(1) || checkCols(1) || checkDi1(1) || checkDi2(1)) { finished = true; return 1;}
        if(checkRows(-1) || checkCols(-1) || checkDi1(-1) || checkDi2(-1)) {finished = true; return -1;}
        if(finished)
            return 0;
        else
            return -2;
    }

    private boolean checkRows(int winner) {
        for (int i = 0; i < 3; i++) {
            if(checkRow(i, winner) == 1) return true;
        }
        return false;
    }

    private boolean checkCols(int winner) {
        for (int i = 0; i < 3; i++) {
            if(checkCol(i, winner) == 1) return true;
        }
        return false;
    }

    private int checkRow(int row, int winner) {
        for (int i = 0; i < 3; i++) {
            if(gameArray[row][i] != winner) return -1;
        }
        return 1;
    }

    private int checkCol(int col, int winner) {
        for (int i = 0; i < 3; i++) {
            if(gameArray[i][col] != winner) return -1;
        }
        return 1;
    }

    private boolean checkDi1(int winner) {
        for (int i = 0; i < 3; i++) {
            if(gameArray[i][i] != winner) return false;
        }
        return true;
    }

    private boolean checkDi2(int winner) {
        for (int i = 0; i < 3; i++) {
            if(gameArray[i][2 - i] != winner) return false;
        }
        return true;
    }
}
